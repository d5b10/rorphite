# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Metric.delete_all
m1 = MetricDirectory.new(name: "test")
m1.save
m2 = MetricLeaf.new(name: "foo", values_per_seconds: 1, parent_id: m1.id )
m2.save
m3 = MetricLeaf.new(name: "sine", values_per_seconds:1, parent_id: m1.id )
m3.save
m4 = MetricLeaf.new(name: "cosine", values_per_seconds:1, parent_id: m1.id )
m4.save
d2 = MetricDirectory.new( name: "numbers", parent_id: m1.id )
d2.save
m5 = MetricLeaf.new(name: "one", values_per_seconds:1, parent_id: d2.id )
m5.save
m6 = MetricLeaf.new(name: "two", values_per_seconds:1, parent_id: d2.id )
m6.save

m7 = MetricDirectory.new( name: "servers", parent_id: m1.id )
m7.save
srv_metrics = Array.new(50)
(0..49).each do |i|
  srv_metrics[i] = MetricLeaf.new( name: "srv#{i}", values_per_seconds: 1, parent_id: m7.id )
  srv_metrics[i].save
end

# generate data for the past hour
Value.delete_all
t = Time.now.to_i
(t - 3600 .. t).each do |i|
  ts = Time.at(i)
  Value.new( metric: m2, value: 0, ts: ts ).save
  Value.new( metric: m3, value: 10 + 10 * Math.sin(((i - (t-3600).to_f) / 3600) * Math::PI * 2), ts: ts ).save
  Value.new( metric: m4, value: 10 + 10 * Math.cos(((i - (t-3600).to_f) / 3600) * Math::PI * 2), ts: ts ).save
  Value.new( metric: m5, value: 1, ts: ts ).save
  Value.new( metric: m6, value: 2, ts: ts ).save

  (0..49).each do |i|
    Value.new( metric: srv_metrics[i], value: 9500+rand(1000), ts: ts).save
  end
end

# generate some pseudo traffic of http servers

