class AddTypeToMetrics < ActiveRecord::Migration
  def change
    add_column :metrics, :type, :string
  end
end
