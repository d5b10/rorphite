class AddPathToMetrics < ActiveRecord::Migration
  def change
    add_column :metrics, :path, :string
  end
end
