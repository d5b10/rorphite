class MetricUniqueIndexOnNameAndParentId < ActiveRecord::Migration
  def change
    add_index :metrics, [ :parent_id, :name ], unique: true
  end
end
