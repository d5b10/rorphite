class AddIndexOnTsToValues < ActiveRecord::Migration
  def change
    add_index :values, [ :ts ]
  end
end
