class AddParentId < ActiveRecord::Migration
  def change
    add_column :metrics, :parent_id, :integer
  end
end
