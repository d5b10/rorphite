class CreateValues < ActiveRecord::Migration
  def change
    create_table :values do |t|
      t.references :metric, index: true, foreign_key: true
      t.float :value
      t.datetime :ts
    end
  end
end
