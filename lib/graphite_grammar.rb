#require 'parslet'

class GraphiteGrammar < Parslet::Parser
  rule(:symbol) { match('[a-zA-Z0-9_*]').repeat(1) }
  rule(:metric_name) { (symbol >> ( str('.') >> symbol ).repeat).as(:metric_name) }
  rule(:func_call) { (symbol.as(:func_name) >> str('(') >> arg_list.as(:args) >> str(')')).as(:func_call) }
  rule(:expression) { func_call | metric_name }
  rule(:stringliteral) { str('"') >> match("[^\"]").repeat(1).as(:stringliteral) >> str('"') }
  rule(:arg_list) { ((expression|stringliteral).as(:first) >> (str(',') >> arg_list).repeat.as(:rest)) }
  root(:expression)
end
