#require 'parslet'

class GraphiteTransform < Parslet::Transform

  attr_accessor :time_from, :time_until

  def initialize( time_from, time_until )
    @time_from = time_from
    @time_until = time_until
    super()
  end

  # flatten the recursive argument list
  rule( :first => subtree(:a1), :rest => subtree(:r) ) { [ a1, r ].flatten }

  # handle metric references (these are the leaves of the tree)
  rule( :metric_name => subtree(:mn) ) { |dict|
    path = dict[:mn].to_s.gsub(/\*/, "%")
    path_expression = (path == dict[:mn]) ? "path = '#{path}'" : "path LIKE '#{path}'"
    MetricsQuery.new( where: [
      path_expression,
## this does not work for whatever reason:
#      "ts BETWEEN '#{@time_from}' AND '#{@time_until}'"
    ] )
  }

  # handle function calls (this "sequence" only matches after metrics and stringliterals inside
  # have already been processed
  rule( :func_name => simple(:fn), :args => sequence(:a) ) { a[0].handle_function_call(fn, a[1]) }

  # transform "{ :strinliteral => x }" into x, so that the arguments of a function become
  # a sequence as expected by the func_name rule above
  rule( :stringliteral => simple(:sl) ) { sl }

  # remove func calls whose args have already been processed
  rule( :func_call => simple(:x) ) { x }
end
