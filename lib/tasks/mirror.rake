task :mirror => :environment do
  block = 0
  TCPSocket.open("localhost", 2003) do |sock|
    while true
      puts "block #{block}"
      rows = Value.all.offset(block*1000).limit(1000)
      rows.each { |v| sock.send("#{v.metric.path} #{v.value} #{v.ts.to_i}\n",0) } 
      if rows.length == 1000 then
        block+=1
      else
        break
      end
    end
  end
end

