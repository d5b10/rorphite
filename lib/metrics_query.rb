class MetricsQuery
  attr_accessor :select_target, :select_value, :select_ts, :relation, :with, :where, :group_by
  attr_reader :max_temp_query_num

  def initialize( params, max_temp_query_num=1 )
    @select_target = params[:select_target] || "path"
    @select_value = params[:select_value] || "value"
    @select_ts = params[:select_ts] || "ts"
    @relation = params[:relation] || "my_metrics"
    @with = params[:with] || [ { name: "my_metrics", text: "SELECT path, value, ts FROM metrics m JOIN \"values\" ON m.id = metric_id" } ]
    @where = params[:where] || []
    @group_by = params[:group_by] 

    @max_temp_query_num = max_temp_query_num
  end

  def to_sql
    res = "WITH " + @with.map { |h| "#{h[:name]} AS (#{h[:text]})" }.join(", ")+" "
    res += to_sql_without_with
  end

  def to_sql_without_with
    "SELECT #{@select_target} AS path, #{@select_value} AS value, #{@select_ts} AS ts FROM #{@relation} " + (@where.empty? ? "" : "WHERE ") + where.join(" AND ") + (@group_by.nil? ? "" : " GROUP BY #{@group_by}") + " ORDER BY 1, 3"
  end

  def push_current_query
    @max_temp_query_num += 1
    t = "t#{@max_temp_query_num.to_s}"
    @with.push({ name: t, text: to_sql_without_with })
    @relation = t # this must happen after the above call to to_sql_without_with (to prevent loops)
    @select_target = "path"
    @select_value = "value"
    @ts = "ts"
    @where = []
    @group_by = nil

    @relation
  end

  def handle_function_call(fn,data="")
    case fn.to_s.downcase 
      when "absolute"
        @select_target= "'absolute('||#{@select_target}||')'"
        @select_value= "ABS(#{@select_value})"
      when "sumseries"
        @select_target= "'sumSeries('||#{@select_target}||')'"
        @select_value= "SUM(#{@select_value})"
        # the third column is always the timestamp
        group_by= 3
      when "aspercent"
        t1 = push_current_query
        @select_target = "'sum'"
        @select_value = "SUM(value)"
        @group_by = "1,3"
        t2 = push_current_query
        @select_target= "'asPercent('||#{t1}.path||')'"
        @select_value= "(#{t1}.value / #{t2}.value) * 100"
        @select_ts= "#{t1}.ts"
        @relation= "#{t1} JOIN #{t2} ON #{t1}.ts = #{t2}.ts"
      when "alias"
        # I'm not sure if this is a correct implementation, because it changes the way data is
        # displayed instead of just the legend. maybe it should actually do nothing.
        @select_target = "'#{data}'"
      else
        raise "function #{fn} is not implemented"
    end
    self
  end
end
