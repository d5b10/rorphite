class MetricController < ApplicationController
  def find
    render json: { metrics: Metric.all.map { |m| { is_leaf: (m.is_a?(MetricLeaf) ? 1 : 0), name: m.name, path: m.path } } }
  end
end
