require 'graphite_grammar.rb'
require 'graphite_transform.rb'
require 'metrics_query.rb'

class RenderController < ApplicationController
  def render_raw

    if params[:format] != "json"
      raise "Only json is supported as format!"
    end

    time_from = time_param_to_epoch( params[:from], Time.now - 3600 )
    time_until = time_param_to_epoch( params[:until], Time.now )

    parse_tree = GraphiteGrammar.new.parse(params[:target])
    query = GraphiteTransform.new(time_from, time_until).apply(parse_tree).to_sql

    res = ActiveRecord::Base.connection.execute(query)
    # we rely on "ORDER BY path" here!
    response = []
    last_path = nil
    res.each { |e|
      if last_path.nil? || last_path != e["path"]
        response.push  target: e["path"], datapoints: [] 
      end
      # e[1] is the name-agnostic form of e["value"] (which changes if SQL-functions are
      # applied to value) - using "AS" in the query is probably nicer but might break
      response[-1][:datapoints].push [ e["value"], Time.parse(e["ts"]).to_i ]
      last_path = e["path"]
    }
    
    render json: response
  end

  private
    def time_param_to_epoch(param, default)

      time_units = {
        s:        1,
        min:     60,
        h:     3600,
        d:    86400,
        w:   604800,
        mon:2592000,
        y: 31536000
      }

      if param.nil? || param == "now"
        param = default
      end
      if param =~ /(?<sign>[+-])(?<number>\d+)(?<unit>[a-zA-Z])/
        logger.debug "number = #{$~[:number]}, unit = #{$~[:unit]}"
        time_from = Time.now - $~[:number].to_i * time_units[$~[:unit].to_sym]
      else
        time_from = Time.at( param.to_i )
      end
    end
end
