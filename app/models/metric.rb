require 'acts_as_tree'

class Metric < ActiveRecord::Base
  validates_uniqueness_of :name, scope: :parent_id
  acts_as_tree
  before_save :set_path

  def set_path
    self.path = ""
    if !parent.nil?
      self.path += parent.path.nil? ? "" : parent.path + "."
    end
    self.path += name
  end
end

