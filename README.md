# rorphite - a drop-in replacement for the graphite JSON API with relational storage


Warning: this software is far from production-ready. Use it for hacking, but nothing else right now.


## Intro


rorphite aims to be a replacement for the graphite backend when it is accessed through the JSON API (e.g. by grafana). 
As the name implies, it is a graphite written in Ruby on Rails (RoR) and accesses its data through ActiveRecord, but actually 
it only implements the parts of graphite that are required to run grafana as a frontend. So it will probably never generate its 
own PNGs or other image formats. Currently it does not even have a interface to send real metrics there, but I only test with data
imported directly into the database.


## Motivation


So why would you want something like this? My motivation is that we had severe problems with a huge graphite installation which was
stored on an NFS share. Since graphite uses whisper files to store its data, the latency of the storage added up to significant delays
on dashboards that accessed many of them, which made many users very unhappy. Of course I have heard of InfluxDB, OpenTSDB and others, 
but since I was not eager to add more new technology at that time. So I was wondering: what would happen if this was a relational 
storage where I know what to do if it does not behave so well? It took me almost a year to come up with this small side project to 
find the answer (which I still don’t know because I have not done extensive tests yet).


In case you wonder why I did not write just a graphite storage plugin: graphite implements the logic of parsing and executing the 
function calls that you can perform with your data, but the actual data access (which can be implemented by a plugin) is pretty dumb: 
you can only choose where to put the data and then read it and return it in your API. What I wanted was to push the execution of the 
graphite expressions (e.g. sumSeries, aggregate functions …) down to the SQL-database, so writing a plugin for graphite is simply not 
enough (furthermore I wanted to learn Ruby, not Python :)


## Status


You can start rorphite (I only tested “rails s” so far, but you can probably deploy it like any Rails app) and access it’s API with the 
same parameters as the graphite JSON API (e.g. by pointing a grafana installation to it). However, not all the graphite functions are implemented as of now 
(see [this function](lib/metrics_query.rb?at=master&fileviewer=file-view-default#metrics_query.rb-40) for the list of functions 
which are already there). Since I already implemented the function-parsing and provided a framework for SQL-generation, it should 
be pretty easy to add more functions (most of the ones that I tried only have 4-5 LOC).


As mentioned above, there is currently no way for receive metrics. There should be a daemon which implements the [graphite protocol](http://graphite.readthedocs.io/en/latest/feeding-carbon.html) this and flushes the data to the underlying ActiveRecord database. For the time being, use "rake db:seed" to get some test data into the database (sine waves and some random numbers).


A word on databases: since I wanted to generate optimal SQL, the abstraction of ActiveRecord could not be used. Instead, rorphite
generates SQL itself and sends it to the database through a ActiveRecord::Base.connection. The SQL code that it generates works
on SQLite (version 3.8.3 with support for CTEs) and PostgreSQL. It might work on other databases with common table expressions (CTEs),
but MySQL is not supported for this reason. Sorry.


## SQL-generation


- walk up the parse tree
- if it can be done by adding a function or a group, do it in the query
- if not: define what we already have as a “WITH” subquery and start with a new main query


## Todos

* I did not manage to get the time_from and time_until attributes of GraphiteTransform into the
  rule evaluation, so currently there is no restriction based on the time

* sanitize params which are interpolated into SQL (not possible with ActiveRecord::Base.connection.execute) - e.g. metric names and string literals

* ActiveRecord::Base.execute is not the right tool for the job for various reasons (it returns all rows in an array and it can not sanitize params, see above)

* wildcards in metrics names are not yet fully implemented (* works in the renderer, but I'm
  not sure if is really the same behaviour as in the real graphite w.r.t. greedyness).
  In the metrics API there are not wildcards yet.

* there should be a socket server to read actual metrics into the DB (currently we only work
  with test data)

* the parser does not tolerate whitespaces

* the renderer ignores the maxDataPoints parameter, i.e. there is no aggregation yet

* improve this README

## Author

Feel free to drop me a message:

Daniel Boesswetter <daniel at daniel-boesswetter.de>